<?php

/**
 * @file
 * A handler to provide a Views imagecache external field.
 */

class views_imagecache_External_field_handler extends views_handler_field {

  function query() {
    // Do nothing -- to override the parent query.
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['image_style'] = array('default' => FALSE);
    $options['image_url'] = array('default' => FALSE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['image_style'] = array(
      '#type' => 'select',
      '#title' => t('Image style'),
      '#options' => image_style_options(),
      '#default_value' => $this->options['image_style'],
    );

    $form['image_url'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#default_value' => $this->options['image_url'],
    );
    return $form;
  }

  function render($values) {
    $tokens = $this->get_render_tokens($values);

    $token = $this->options['image_url'];

    if (!isset($tokens[$token]) || !valid_url($tokens[$token])) {
      return;
    }

    if (!empty($this->options['image_style'])) {
      $render = array(
        '#theme' => 'imagecache_external',
        '#path' => $tokens[$token],
        '#style_name' => $this->options['image_style'],
      );
    }
    else {
      $path = imagecache_external_generate_path($tokens[$token]);
      $info = image_get_info($path);

      $render = array(
        '#theme' => 'image',
        '#path' => file_create_url($path),
        '#width' => $info['width'],
        '#height' => $info['height'],
      );
    }
    return render($render);
  }

}
