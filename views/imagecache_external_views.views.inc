<?php
/**
 * @file
 * Views integration file for imagecache_external_views.
 */

/**
 * Implements hook_views_data_alter().
 */
function imagecache_external_views_views_data_alter(&$data) {
  $data['views']['imagecache_external_image'] = array(
    'title' => t('Imagecache external'),
    'help' => t('Adds imagecache external'),
    'field' => array(
      'handler' => 'views_imagecache_external_field_handler',
    ),
  );
}
